from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import cgi
import pyowm

PORT_NUMBER = 8082

#This class will handle incoming requests from
#the browser
class myHandler(BaseHTTPRequestHandler):

    htmlPage = ('<html><head><title>Welcome to my weather page</title></head>'
                            '<body><h1>Welcome</h1><form method="POST" action="">'
                            '<label>Enter your zip code for your forecast: '
                            '</label><input type="text" name="location"/>'
                            '<input type="submit" value="Send"/></form>')


    #Handler for GET requests
    def do_GET(self):
        try:
            #output the html form
            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            self.wfile.write(self.htmlPage)
            self.wfile.write("</body></html>");
            return

        except IOError:
            self.send_error(500,'IO Error: %s' % self.path)

    #Handler for POST requests
    def do_POST(self):
        if self.path=="/":
            form = cgi.FieldStorage(
                fp=self.rfile,
                headers=self.headers,
                environ={'REQUEST_METHOD':'POST',
                         'CONTENT_TYPE':self.headers['Content-Type'],
            })

            #instanciate the weather object
            owm = pyowm.OWM()
            observation = owm.weather_at_place(form["location"].value)
            weather = observation.get_weather()
            winds = weather.get_wind()

            #output the html form
            self.send_response(200)
            self.end_headers()

            self.wfile.write(self.htmlPage)
            self.wfile.write("<img src='http://openweathermap.org/img/w/%s.png'>"
                                                % weather.get_weather_icon_name())
            self.wfile.write("<h5>Currently in %s: "
                                                % (observation.get_location()).get_name())
            self.wfile.write("%s, " % weather.get_detailed_status())
            self.wfile.write("the wind is %s knots from %s degrees.<br />"
                                                % (winds['speed'],winds['deg']))
            self.wfile.write("The temperature is %sF, "
                                                % (weather.get_temperature('fahrenheit')['temp']))
            self.wfile.write("and the humidity is %s percent<h5>"
                                                % weather.get_humidity())
            self.wfile.write("</body></html>");
            return

#Create a web server and define the handler to manage the
#incoming requests
try:
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    print 'Started httpserver on port ' , PORT_NUMBER

    #Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
